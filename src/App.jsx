import React, { Component } from 'react';
import {
  Link,
  Route
} from 'react-router-dom';

import {
  AddArticle
} from './Environment';

export default class App extends Component {
  render() {
    console.log('Render App');
    return (<div>
        <h1>Mos App component!</h1>
        <Route path='/' component={AddArticle} />
      </div>);
  }
}