import React, { Component } from 'react';

export default class Input extends Component {
  render() {
    console.log('Render Input');
    return (
      <form>
        <p>
          First name:
          <input type="text" name="firstname" />
        </p>
        <p>
          Last name:
          <input type="text" name="lastname" />
        </p>
      </form>
      );
  }
}